#
# Be sure to run `pod lib lint SDWebImageSVGKitPlugin.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MobileVLCKit'
  s.version          = '3.3.13'
  s.summary          = 'MobileVLCKit is an Objective-C wrapper for libvlcs external interface on iOS'
  s.authors = ''
  
# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://code.videolan.org/videolan/VLCKit'
  s.source           = { :git => 'https://bitbucket.org/sergey_zuev_2/babyphone-vlc-player.git' }

  s.ios.deployment_target = '8.0'
  s.framework = 'QuartzCore', 'CoreText', 'AVFoundation', 'Security', 'CFNetwork', 'AudioToolbox', 'OpenGLES', 'CoreGraphics', 'VideoToolbox', 'CoreMedia'
  s.libraries = 'c++', 'xml2', 'z', 'bz2', 'iconv'
  s.source_files = 'MobileVLCKit.framework/Headers/*.h'
  s.ios.vendored_frameworks = 'MobileVLCKit.framework'
  s.pod_target_xcconfig = { 'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11', 'CLANG_CXX_LIBRARY' => 'libc++' }
end
